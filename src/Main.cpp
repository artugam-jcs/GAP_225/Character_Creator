// clang-format off
#include <iostream>

class Weapon
{
    std::string m_name;
    int m_strength;
    int m_dexterity;
    int m_magic;

public:
    Weapon(std::string name, int strength, int dexterity, int magic)
    {
        this->m_name = name;
        this->m_strength = strength;
        this->m_dexterity = dexterity;
        this->m_magic = magic;
    }

    void Print()
    {
        std::cout << this->m_name << ": S:" << this->m_strength
                  << ", D:" << this->m_dexterity << ", M:" << this->m_magic
                  << std::endl;
    }

    /* Setter & Getter */
    int GetStrength() { return this->m_strength; }
    int GetDexterity() { return this->m_dexterity; }
    int GetMagic() { return this->m_magic; }
};

class Character
{
    std::string m_name;
    int m_strength;
    int m_dexterity;
    int m_magic;
    Weapon *m_pWeapon;

public:
    Character(std::string name, int strength, int dexterity, int magic,
              Weapon *weapon)
    {
        this->m_name = name;
        this->m_strength = strength;
        this->m_dexterity = dexterity;
        this->m_magic = magic;
        this->m_pWeapon = weapon;
    }

    void PrintName() { std::cout << "Name: " << this->m_name << std::endl; }

    void PrintAttributes()
    {
        std::cout << "S:" << this->m_strength + m_pWeapon->GetStrength()
                  << ", D:" << this->m_dexterity + m_pWeapon->GetDexterity()
                  << ", M:" << this->m_magic + m_pWeapon->GetMagic()
                  << std::endl;
    }

    void PrintWeapon()
    {
        if (this->m_pWeapon == nullptr)
        {
            std::cout << "No weapon";
            return;
        }
        m_pWeapon->Print();
    }
};

int main(int argc, char *argv[])
{
    std::string name;
    int strength, dexterity, magic, choice;

    std::cout << "Enter name: ";
    std::cin >> name;

    std::cout << "Enter base strength (number): ";
    std::cin >> strength;

    std::cout << "Enter base dexterity (number): ";
    std::cin >> dexterity;

    std::cout << "Enter base magic (number): ";
    std::cin >> magic;

    std::cout << "\nChoose a weapon:" << std::endl;

    Weapon sword = Weapon("Sword", 5, 2, 0);
    Weapon longbow = Weapon("Longbow", 3, 5, 0);
    Weapon staff = Weapon("Staff", 1, 0, 5);

    std::cout << "0) ";
    sword.Print();

    std::cout << "1) ";
    longbow.Print();

    std::cout << "2) ";
    staff.Print();

    std::cout << "3) No weapon" << std::endl;

    std::cout << "\nEnter the number of your choice: ";
    std::cin >> choice;

    system("cls");

    Weapon *chosen = nullptr;

    switch (choice)
    {
        case 0:
            chosen = &sword;
            break;
        case 1:
            chosen = &longbow;
            break;
        case 2:
            chosen = &staff;
            break;
    }

    Character newCharacter =
        Character(name, strength, dexterity, magic, chosen);

    newCharacter.PrintName();
    newCharacter.PrintAttributes();
    newCharacter.PrintWeapon();

    return 0;
}
