# 1.1 [Assignments] Character Creator

Build with CMake:

```
# Navigate to project directory
cd /path/to/project/

# Build with CMake, the build folder will be created automatically
cmake -S . -B build
```
